import React, { Component, Fragment } from 'react'
import { Header, Form, Item, Content, Label, Input, Text, Body, Title, Left, Right } from 'native-base'
import { CalculateButton, AmountText, StyledContainer } from './styles'
export default class SegmentExample extends Component {
  constructor (props) {
    super(props)
    this.state = {
      percentage: 0,
      limit: 0,
      amount: 0,
      noLimit: false
    }
  }

  handlePercentage = (text) => {
    this.setState({ percentage: text })
  }

  handleLimit = (text) => {
    this.setState({ limit: text })
  }

  handleCalculate= () => {
    const { percentage, limit } = this.state
    if (!limit || limit === '') {
      this.setState({ noLimit: true })
    } else {
      const max = limit / (percentage / 100)
      this.setState({
        noLimit: false,
        amount: max
      })
    }
  }

  render () {
    return (
      <StyledContainer>
        <Header>
          <Left />
          <Body>
            <Title>Cashini</Title>
          </Body>
          <Right />
        </Header>

        <Content padder>
          <Text>Let's calculate</Text>
          <Form>
            <Item stackedLabel>
              <Label>How much is the cashback?</Label>
              <Input autoFocus keyboardType='numeric' returnKeyType='next' onChangeText={this.handlePercentage} />
            </Item>
            <Item stackedLabel>
              <Label>Maximum cashback</Label>
              <Input keyboardType='numeric' returnKeyType='done' onChangeText={this.handleLimit} />
            </Item>
            <CalculateButton block large>
              <Text onPress={this.handleCalculate}>Calculate!</Text>
            </CalculateButton>
            {this.state.noLimit
              ? (<Text>
              Lets buy all everything! there's no limit
              </Text>)
              : (
                <Fragment>
                  <Text>Recomended purchase value</Text>
                  <AmountText>
                    {this.state.amount}
                  </AmountText>
                </Fragment>
              )
            }
          </Form>
        </Content>
      </StyledContainer>
    )
  }
}
