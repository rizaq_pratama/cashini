import styled from 'styled-components'
import { Button, H1, Container } from 'native-base'

export const CalculateButton = styled(Button)`
    text-align: center;
    margin-top: 10;
`
export const AmountText = styled(H1)`
    margin-top: 20;
    color: green;
`
export const StyledContainer = styled(Container)`
    background-color: #fefef0;    
`
